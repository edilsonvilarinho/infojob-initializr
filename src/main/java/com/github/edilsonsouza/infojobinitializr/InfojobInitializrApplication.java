package com.github.edilsonsouza.infojobinitializr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfojobInitializrApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfojobInitializrApplication.class, args);
	}

}
