package com.github.edilsonsouza.infojobinitializr.app.mvc.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateProject {
    private final static String PASTA_SRC = "/src";
    private final static String ARQUIVO_POM_XML = "pom.xml";
    private final static String PASTA_MAIN = "/main";
    private final static String PASTA_TESTE = "/test";
    private final static String PASTA_JAVA = "/java";
    private final static String PASTA_RESOURCES = "/resources";
    private final static String PASTA_CLIENT = "/client";
    private final static String PASTA_CONTROLLER = "/controller";
    private final static String PASTA_ENTITY = "/entity";
    private final static String PASTA_DAO = "/dao";
    private final static String PASTA_INFRA = "/infra";
    private final static String PASTA_CONFIGURACAO = "/configuracao";
    private final static String PASTA_JOB = "/job";
    private final static String PASTA_MODEL = "/model";
    private final static String ARQUIVO_MAIN_JAVA = "Main.java";
    private final static String ARQUIVO_LISTA_MAPPING_HIBERNATE_IMPL_JAVA = "ListaMappingHibernateImpl.java";
    private final static String ARQUIVO_MAP_INFO_JOB_PROPERTIES_JAVA = "MapInfoJobProperties.java";
    private final static String ARQUIVO_MAIN_JOB_JAVA = "MainJob.java";
    private final static String ARQUIVO_GITIGNORE = "!.gitignore";
    private final static String ARQUIVO_LOG4J = "log4j.properties";

    private final static String GROUP_ID_INFO_JOB_KERNEL = "br.com.informata.infojobkernel";
    private final static String ARTIFACT_ID_INFO_JOB_KERNEL = "InfoJobKernel";
    private final static String VERSION_JOB_KERNEL = "14.07.00.00";

    //private final String PASTA_USER_INFOFOB_INITIALIZR = "/home/infojob-initializr/";
    private String PASTA_USER_INFOFOB_INITIALIZR;

    private Job job;

    private String PASTA_PROJETO = File.separator;
    private String GROUP_ID;
    private String ARTIFACT_ID;
    private String VERSION;
    private String NAME;
    private String NOME_JOB;
    private String NOME_GRUPO;
    private String NOME_JAR;
    private String GRUPO_JAR;

    private String PASTA_TEMPORARIA = "";

    public CreateProject(Job job) {
        this.job = job;
    }

    private static void conteudoPomXml(String caminhoPomXml, String groupId, String artifactId, String version, String name, String mainClass, String groupIdInfojobkernel, String artifactIdInfojobkernel, String versionInfojobkernel) throws FileNotFoundException {
        String POM_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">\n"
                + "    <modelVersion>4.0.0</modelVersion>\n"
                + "    <groupId>" + groupId + "</groupId>\n"
                + "    <artifactId>" + artifactId + "</artifactId>\n"
                + "    <version>" + version + "</version>\n"
                + "    <packaging>jar</packaging>\n"
                + "    <properties>\n"
                + "        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>\n"
                + "        <maven.compiler.source>1.8</maven.compiler.source>\n"
                + "        <maven.compiler.target>1.8</maven.compiler.target>\n"
                + "    </properties>\n"
                + "    <name>" + name + "</name>\n"
                + "    <dependencies>\n"
                + "        <dependency>\n"
                + "            <groupId>" + groupIdInfojobkernel + "</groupId>\n"
                + "            <artifactId>" + artifactIdInfojobkernel + "</artifactId>\n"
                + "            <version>" + versionInfojobkernel + "</version>\n"
                + "        </dependency> \n"
                + "        <dependency>\n"
                + "            <groupId>org.apache.commons</groupId>\n"
                + "            <artifactId>commons-lang3</artifactId>\n"
                + "            <version>3.8.1</version>\n"
                + "        </dependency>\n"
                + "    </dependencies>\n"
                + "    <build>\n"
                + "        <finalName>${project.artifactId}</finalName>\n"
                + "        <plugins>\n"
                + "            <plugin>\n"
                + "                <artifactId>maven-assembly-plugin</artifactId>\n"
                + "                <configuration>\n"
                + "                    <archive>\n"
                + "                        <manifest>\n"
                + "                            <mainClass>" + mainClass + "</mainClass>\n"
                + "                        </manifest>\n"
                + "                    </archive>\n"
                + "                    <descriptorRefs>\n"
                + "                        <descriptorRef>jar-with-dependencies</descriptorRef>\n"
                + "                    </descriptorRefs>\n"
                + "                </configuration>\n"
                + "                <executions>\n"
                + "                    <execution>\n"
                + "                        <phase>package</phase>\n"
                + "                        <goals>\n"
                + "                            <goal>single</goal>\n"
                + "                        </goals>\n"
                + "                    </execution>\n"
                + "                </executions>\n"
                + "            </plugin>\n"
                + "            <plugin>\n"
                + "                <artifactId>maven-war-plugin</artifactId>\n"
                + "                <version>3.0.0</version>\n"
                + "            </plugin>\n"
                + "        </plugins>\n"
                + "    </build>\n"
                + "</project>";

        PrintWriter stlArquivo = new PrintWriter(caminhoPomXml);
        stlArquivo.write(POM_XML);
        stlArquivo.close();
    }

    private static int quantidadeArquivos(String dir) throws IOException {
        int cont = 0;
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isDirectory()) {
                    cont++;
                    if (cont == 99999) {
                        cont = 0;
                    }
                }
            }
        } else {
            throw new IOException("Acesso negado");
        }
        return cont;
    }

    private void conteudoMain(String caminhoMain, String groupId, String nomeJob, String nomeGrupo, String nomeJar, String grupoJar) throws FileNotFoundException {
        String POM_XML = "package " + groupId + ";\n"
                + "\n"
                + "import " + groupId + ".infra.configuracao.ListaMappingHibernateImpl;\n"
                + "import " + groupId + ".infra.configuracao.MapInfoJobProperties;\n"
                + "import " + groupId + ".job.MainJob;\n"
                + "\n"
                + "import br.com.informata.infojobkernel.cron.InfoJobCron;\n"
                + "import br.com.informata.infojobkernel.infra.AbstractMain;\n"
                + "import org.quartz.Scheduler;\n"
                + "import org.quartz.SchedulerException;\n"
                + "import org.quartz.impl.StdSchedulerFactory;\n"
                + "\n"
                + "public class Main extends AbstractMain {\n"
                + "\n"
                + "    public static String nomeJar;\n"
                + "    public static String grupoJar;\n"
                + "\n"
                + "    public static void main(String[] args) {\n"
                + "\n"
                + "        try {\n"
                + "            Main main = new Main();\n"
                + "            main.setNomeGrupoJar(args);\n"
                + "            main.executaConfiguracaoBanco(null,\n"
                + "                    main.nomeJar,\n"
                + "                    new ListaMappingHibernateImpl(),\n"
                + "                    new MapInfoJobProperties());\n"
                + "            String caminho = main.setConfiguracoesQuartzSystem();\n"
                + "            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory(caminho);\n"
                + "            Scheduler scheduler = schedulerFactory.getScheduler();\n"
                + "            InfoJobCron infoJobCronClonar = new InfoJobCron(nomeJar, " + "\"" + nomeJob + "\"" + "," + "\"" + nomeGrupo + "\"" + ");\n"
                + "            scheduler = infoJobCronClonar.configuraSchedulerPorJobGrupo(new MainJob(), scheduler);\n"
                + "            scheduler.start();\n"
                + "    \n"
                + "        } catch (SchedulerException ex) {\n"
                + "            log.error(\"Erro.\", ex);\n"
                + "        }\n"
                + "    }\n"
                + "\n"
                + "    @Override\n"
                + "    protected void setNomeGrupoJar(String[] args) {\n"
                + "        if (args.length >= 2) {\n"
                + "            nomeJar = args[0];\n"
                + "            grupoJar = args[1];\n"
                + "        } else {\n"
                + "            nomeJar = " + "\"" + nomeJar + "\"" + ";\n"
                + "            grupoJar = " + "\"" + grupoJar + "\"" + ";\n"
                + "        }\n"
                + "    }\n"
                + "\n"
                + "}";
        PrintWriter stlArquivo = new PrintWriter(caminhoMain);
        stlArquivo.write(POM_XML);
        stlArquivo.close();
    }

    private void conteudoMainJob(String caminhoMain, String groupId) throws FileNotFoundException {
        String POM_XML = "package " + groupId + " .job;\n"
                + "\n"
                + "import " + groupId + ".Main;\n"
                + "import br.com.informata.infojobkernel.job.AbstractJob;\n"
                + "import org.quartz.DisallowConcurrentExecution;\n"
                + "\n"
                + "import org.quartz.JobExecutionContext;\n"
                + "\n"
                + "@DisallowConcurrentExecution\n"
                + "public class MainJob extends AbstractJob {\n"
                + "\n"
                + "    @Override\n"
                + "    protected void setNomeGrupoJar() {\n"
                + "        super.nomeJar = Main.nomeJar;\n"
                + "        super.grupoJar = Main.grupoJar;\n"
                + "    }\n"
                + "\n"
                + "    @Override\n"
                + "    protected void processaOperacao(JobExecutionContext jec) {\n"
                + "    }\n"
                + "\n"
                + "}";
        PrintWriter stlArquivo = new PrintWriter(caminhoMain);
        stlArquivo.write(POM_XML);
        stlArquivo.close();
    }

    private void conteudoListaMappingHiberneteImpl(String caminhoListaMappingHiberneteImpl, String groupId) throws FileNotFoundException {
        String POM_XML = "package " + groupId + ".infra.configuracao;\n"
                + "\n"
                + "import br.com.informata.infojobkernel.infra.AbstractListaConfiguracoes;\n"
                + "\n"
                + "/**\n"
                + " *\n"
                + " * @author bruno.queiroz\n"
                + " */\n"
                + "public class ListaMappingHibernateImpl extends AbstractListaConfiguracoes {\n"
                + "\n"
                + "    public ListaMappingHibernateImpl() {\n"
                + "        super(true, true);\n"
                + "    }\n"
                + "\n"
                + "}";
        PrintWriter stlArquivo = new PrintWriter(caminhoListaMappingHiberneteImpl);
        stlArquivo.write(POM_XML);
        stlArquivo.close();
    }

    private void conteudoMapInfoJobProperties(String caminhoListaMappingHiberneteImpl, String groupId) throws FileNotFoundException {
        String POM_XML = "/*\n"
                + " * To change this license header, choose License Headers in Project Properties.\n"
                + " * To change this template file, choose Tools | Templates\n"
                + " * and open the template in the editor.\n"
                + " */\n"
                + "package " + groupId + ".infra.configuracao;\n"
                + "\n"
                + "import br.com.informata.infojobkernel.infra.AbstractMapConfiguracoes;\n"
                + "import br.com.informata.infojobkernel.infra.NomesParametrosProperties;\n"
                + "\n"
                + "/**\n"
                + " *\n"
                + " * @author bruno.queiroz\n"
                + " */\n"
                + "public class MapInfoJobProperties extends AbstractMapConfiguracoes {\n"
                + "\n"
                + "    public MapInfoJobProperties() {\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_URL, \"jdbc:oracle:thin:@192.168.0.228:1521:NEGRAOPD\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME, \"INTEGRA\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD, \"integra\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_DIALECT, \"org.hibernate.dialect.Oracle10gDialect\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_DRIVER_CLASS, \"oracle.jdbc.driver.OracleDriver\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_C3P0_MIN_SIZE, \"1\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_C3P0_MAX_SIZE, \"5\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_C3P0_TIMEOUT, \"10\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_C3P0_MAX_STATEMENTS, \"50\");\n"
                + "        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD_ENCRIPTADO, \"0\");\n"
                + "    }\n"
                + "}";
        PrintWriter stlArquivo = new PrintWriter(caminhoListaMappingHiberneteImpl);
        stlArquivo.write(POM_XML);
        stlArquivo.close();
    }

    public void create() {
        montarDadosJob(job.getPROJETO_NAME());
        PASTA_PROJETO += job.getPROJETO_NAME();
        //new DeleteFileFolder(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA  + File.separator + job.getPROJETO_NAME() + ".zip");
        //CRIANDO PASTA HOME DO USUARIO
        File f = new File(System.getProperty("user.home") + File.separator + "infojob-initializr");
        if (!f.exists()) {
            f.mkdir();
        }

        PASTA_USER_INFOFOB_INITIALIZR = System.getProperty("user.home") + File.separator + "infojob-initializr" + File.separator;
        //CRIANDO PASTA TEMPORARIA
        try {
            PASTA_TEMPORARIA = quantidadeArquivos(PASTA_USER_INFOFOB_INITIALIZR) + File.separator;
            new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA).mkdir();
        } catch (IOException e) {
            e.printStackTrace();
        }
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO).mkdir();
        //CRIANDO PASTA PROJETO
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO).mkdir();
        //CRIANDO PASTA SRC
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC).mkdir();
        //CRIANDO O POM.XML
        try {
            File file = new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + File.separator + ARQUIVO_POM_XML);
            file.createNewFile();
            conteudoPomXml(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + File.separator + ARQUIVO_POM_XML,
                    GROUP_ID,
                    ARTIFACT_ID,
                    VERSION,
                    NAME,
                    GROUP_ID + "." + "Main",
                    GROUP_ID_INFO_JOB_KERNEL,
                    ARTIFACT_ID_INFO_JOB_KERNEL,
                    VERSION_JOB_KERNEL);
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }

        //CRIANDO PASTA MAIN
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_MAIN).mkdir();

        //CRIANDO PASTA TESTE
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_TESTE).mkdir();

        //CRIANDO PASTA JAVA MAIN
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_MAIN + PASTA_JAVA).mkdir();

        //CRIANDO PASTA JAVA JAVA MAIN RESOURCES
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_MAIN + PASTA_RESOURCES).mkdir();
        criarArquivolog4j(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_MAIN + PASTA_RESOURCES);

        //CRIANDO PASTA JAVA TEST
        new File(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_TESTE + PASTA_JAVA).mkdir();

        String[] strings = GROUP_ID.split("\\.");

        //CRIANDO PASTAS COM BASE NO GROUP ID PROJETO PASTA JAVA MAIN
        String pathMain = PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_MAIN + PASTA_JAVA;
        for (String pasta : strings) {
            pathMain += File.separator + pasta;
            new File(pathMain).mkdir();
        }

//        //CRIANDO PASTA JAVA MAIN PASTA_CLIENT
//        new File(pathMain + PASTA_CLIENT).mkdir();
//        criarArquivoGitignore(pathMain + PASTA_CLIENT);

        //CRIANDO PASTA JAVA PASTA_CONTROLLER
        new File(pathMain + PASTA_ENTITY).mkdir();
        criarArquivoGitignore(pathMain + PASTA_ENTITY);

        //CRIANDO PASTA JAVA PASTA_CONTROLLER
        new File(pathMain + PASTA_CONTROLLER).mkdir();
        criarArquivoGitignore(pathMain + PASTA_CONTROLLER);

        //CRIANDO PASTA JAVA PASTA_DAO
        new File(pathMain + PASTA_DAO).mkdir();
        criarArquivoGitignore(pathMain + PASTA_DAO);

        //CRIANDO PASTA JAVA PASTA_INFRA
        new File(pathMain + PASTA_INFRA).mkdir();
        criarArquivoGitignore(pathMain + PASTA_INFRA);

        //CRIANDO PASTA JAVA PASTA_INFRA PASTA_CONFIGURACAO
        new File(pathMain + PASTA_INFRA + PASTA_CONFIGURACAO).mkdir();
        criarArquivoGitignore(pathMain + PASTA_INFRA + PASTA_CONFIGURACAO);

        //CRIANDO O ARQUIVO_LISTA_MAPPING_HIBERNATE_IMPL_JAVA
        try {
            File file = new File(pathMain + PASTA_INFRA + PASTA_CONFIGURACAO + File.separator + ARQUIVO_LISTA_MAPPING_HIBERNATE_IMPL_JAVA);
            file.createNewFile();
            conteudoListaMappingHiberneteImpl(pathMain + PASTA_INFRA + PASTA_CONFIGURACAO + File.separator + ARQUIVO_LISTA_MAPPING_HIBERNATE_IMPL_JAVA, GROUP_ID);
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }

        //CRIANDO O ARQUIVO_MAP_INFO_JOB_PROPERTIES_JAVA
        try {
            File file = new File(pathMain + PASTA_INFRA + PASTA_CONFIGURACAO + File.separator + ARQUIVO_MAP_INFO_JOB_PROPERTIES_JAVA);
            file.createNewFile();
            conteudoMapInfoJobProperties(pathMain + PASTA_INFRA + PASTA_CONFIGURACAO + File.separator + ARQUIVO_MAP_INFO_JOB_PROPERTIES_JAVA, GROUP_ID);
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }

        //CRIANDO PASTA JAVA PASTA_JOB
        new File(pathMain + PASTA_JOB).mkdir();

        //CRIANDO O ARQUIVO_MAIN_JOB_JAVA
        try {
            File file = new File(pathMain + PASTA_JOB + File.separator + ARQUIVO_MAIN_JOB_JAVA);
            file.createNewFile();
            conteudoMainJob(pathMain + PASTA_JOB + File.separator + ARQUIVO_MAIN_JOB_JAVA, GROUP_ID);
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }

        //CRIANDO PASTA JAVA PASTA_MODEL
        new File(pathMain + PASTA_MODEL).mkdir();
        criarArquivoGitignore(pathMain + PASTA_MODEL);

        //CRIANDO O MAIN.JAVA
        try {
            File file = new File(pathMain + File.separator + ARQUIVO_MAIN_JAVA);
            file.createNewFile();
            conteudoMain(pathMain + File.separator + ARQUIVO_MAIN_JAVA, GROUP_ID, NOME_JOB, NOME_GRUPO, NOME_JAR, GRUPO_JAR);
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }

        //CRIANDO PASTAS COM BASE NO GROUP ID PROJETO PASTA JAVA TEST
        String pathTest = PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO + PASTA_SRC + PASTA_MAIN + PASTA_JAVA;
        for (String pasta : strings) {
            pathTest += File.separator + pasta;
            new File(pathTest).mkdir();
        }

        //ZIP PROJECT
        ZipHelper zippy = new ZipHelper();
        try {
            zippy.zipDir(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + PASTA_PROJETO, PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA + File.separator + job.getPROJETO_NAME() + ".zip");
        } catch (IOException e2) {
            System.err.println(e2);
        }
        //DELETE FOLDER PROJECT
        //new DeleteFileFolder(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA  + PASTA_PROJETO);
        //DELETE ZIP
        //new DeleteFileFolder(PASTA_USER_INFOFOB_INITIALIZR + PASTA_TEMPORARIA  + File.separator + job.getPROJETO_NAME() + ".zip");
    }

    private void montarDadosJob(String projeto_name) {
        GROUP_ID = "br.com.informata.infojob." + projeto_name.toLowerCase();
        ARTIFACT_ID = projeto_name;
        VERSION = "0.0.1";
        NAME = projeto_name;
        NOME_JOB = projeto_name;
        NOME_GRUPO = "grupo" + projeto_name;
        NOME_JAR = projeto_name;
        GRUPO_JAR = "jarsLinhaComando";
    }

    private void criarArquivoGitignore(String path) {
        String conteudoGitignore = "!.gitignore";
        try {
            File file = new File(path + File.separator + ARQUIVO_GITIGNORE);
            file.createNewFile();
            PrintWriter stlArquivo = new PrintWriter(path + File.separator + ARQUIVO_GITIGNORE);
            stlArquivo.write(conteudoGitignore);
            stlArquivo.close();
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void criarArquivolog4j(String path) {
        String conteudolog4j = "log4j.rootLogger=INFO, consoleAppender\n" +
                "\n" +
                "log4j.appender.consoleAppender=org.apache.log4j.ConsoleAppender\n" +
                "log4j.appender.consoleAppender.layout=org.apache.log4j.PatternLayout\n" +
                "log4j.appender.consoleAppender.layout.ConversionPattern=[%t] %-5p %c %x - %m%n";
        try {
            File file = new File(path + File.separator + ARQUIVO_LOG4J);
            file.createNewFile();
            PrintWriter stlArquivo = new PrintWriter(path + File.separator + ARQUIVO_LOG4J);
            stlArquivo.write(conteudolog4j);
            stlArquivo.close();
        } catch (IOException ex) {
            Logger.getLogger(CreateProject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPASTA_TEMPORARIA() {
        return PASTA_TEMPORARIA;
    }
}
