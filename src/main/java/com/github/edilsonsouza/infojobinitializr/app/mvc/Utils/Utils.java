package com.github.edilsonsouza.infojobinitializr.app.mvc.Utils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class Utils {
    private final static String PATH_RESOURCES = "/src/main/resources";

    public static String getPathResources() {
        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString();
        return path += PATH_RESOURCES;
    }

    public static void listPath(String path) {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File " + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
    }
}
