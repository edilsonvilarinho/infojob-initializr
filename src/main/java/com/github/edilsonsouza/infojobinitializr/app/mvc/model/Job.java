package com.github.edilsonsouza.infojobinitializr.app.mvc.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Job {

    @NotBlank(message = "O Nome projeto deve ser informado!")
    @Size(min = 1, max = 60, message = "O Nome projeto deve ter entre {min} e {max} caracteres")
    private String PROJETO_NAME;

    //pom.xml
//    @NotBlank(message = "O Group id deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Group id deve ter entre {min} e {max} caracteres")
//    private String GROUP_ID;
//    @NotBlank(message = "O Artifact id deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Artifact id deve ter entre {min} e {max} caracteres")
//    private String ARTIFACT_ID;
//    @NotBlank(message = "O Version deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Version deve ter entre {min} e {max} caracteres")
//    private String VERSION;
//    @NotBlank(message = "O Name deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Name deve ter entre {min} e {max} caracteres")
//    private String NAME;

    //pom.xml dependency kernel infojobkernel
//    private String GROUP_ID_INFO_JOB_KERNEL;
//    private String ARTIFACT_ID_INFO_JOB_KERNEL;
//    private String VERSION_JOB_KERNEL;
    @NotBlank(message = "O Versão InfoJobKernel deve ser informado!")
    @Size(min = 1, max = 60, message = "O Versão InfoJobKernel ter entre {min} e {max} caracteres")
    private String DEPENDENCY_INFO_JOB_KERNEL;

    //dados do job
//    @NotBlank(message = "O Nome job deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Nome job ter entre {min} e {max} caracteres")
//    private String NOME_JOB;
//    @NotBlank(message = "O Nome grupo deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Nome grupo ter entre {min} e {max} caracteres")
//    private String NOME_GRUPO;
//    @NotBlank(message = "O Nome jar deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Nome jar ter entre {min} e {max} caracteres")
//    private String NOME_JAR;
//    @NotBlank(message = "O Grupo jar deve ser informado!")
//    @Size(min = 1, max = 60, message = "O Grupo jar ter entre {min} e {max} caracteres")
//    private String GRUPO_JAR;

    public Job() {
    }

    public Job(@NotBlank(message = "O Nome projeto deve ser informado!") @Size(min = 1, max = 60, message = "O Nome projeto deve ter entre {min} e {max} caracteres") String PROJETO_NAME, @NotBlank(message = "O Versão InfoJobKernel deve ser informado!") @Size(min = 1, max = 60, message = "O Versão InfoJobKernel ter entre {min} e {max} caracteres") String DEPENDENCY_INFO_JOB_KERNEL) {
        this.PROJETO_NAME = PROJETO_NAME;
        this.DEPENDENCY_INFO_JOB_KERNEL = DEPENDENCY_INFO_JOB_KERNEL;
    }

    public String getPROJETO_NAME() {
        return PROJETO_NAME;
    }

    public void setPROJETO_NAME(String PROJETO_NAME) {
        this.PROJETO_NAME = PROJETO_NAME;
    }

    public String getDEPENDENCY_INFO_JOB_KERNEL() {
        return DEPENDENCY_INFO_JOB_KERNEL;
    }

    public void setDEPENDENCY_INFO_JOB_KERNEL(String DEPENDENCY_INFO_JOB_KERNEL) {
        this.DEPENDENCY_INFO_JOB_KERNEL = DEPENDENCY_INFO_JOB_KERNEL;
    }
}
