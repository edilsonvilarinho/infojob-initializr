package com.github.edilsonsouza.infojobinitializr.app.mvc.controller;

import com.github.edilsonsouza.infojobinitializr.app.mvc.model.CreateProject;
import com.github.edilsonsouza.infojobinitializr.app.mvc.model.Job;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class DashBoardController {

    //private final String PASTA_INFOFOB_INITIALIZR_LINUX = "/home/infojob-initializr";
    private final String PASTA_INFOFOB_INITIALIZR_LINUX = "C:\\Users\\edilson.souza\\infojob-initializr";

    @GetMapping("/")
    public String dashboard(Job job) {
        return "dashboard/dashboard";
    }

    @PostMapping("/")
    public String postDashboard(@Valid @ModelAttribute Job job, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "dashboard/dashboard";
        }
        CreateProject project = new CreateProject(job);
        project.create();
        //redirectAttributes.addFlashAttribute("success", "Projeto criado com sucesso " + job.getPROJETO_NAME());
        return "redirect:/download/" + project.getPASTA_TEMPORARIA().replace(File.separatorChar, '_') + "" + job.getPROJETO_NAME();
    }

    @RequestMapping(path = "/download/{PROJETO_NAME}", method = RequestMethod.GET)
    public ResponseEntity download(@PathVariable("PROJETO_NAME") String PROJETO_NAME, RedirectAttributes redirectAttributes) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        File fileDownload = new File(System.getProperty("user.home") + File.separator + "infojob-initializr" + File.separator + PROJETO_NAME.replace('_', File.separatorChar) + ".zip");
        if (fileDownload.exists()) {
            Path path = Paths.get(fileDownload.getAbsolutePath());
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            return ResponseEntity.ok()
                    .headers(headers)
                    .header("Content-Disposition", "attachment; filename=" + fileDownload.getName())
                    .contentLength(fileDownload.length())
                    .contentType(MediaType.parseMediaType("application/zip"))
                    .body(resource);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .headers(headers)
                    .contentLength(fileDownload.length())
                    .contentType(MediaType.parseMediaType("application/text"))
                    .body("Arquivo não existe");
        }
    }
}